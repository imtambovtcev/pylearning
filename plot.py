import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(1, 10, 100)
plt.plot(x, np.sin(x))
plt.show()

def f(x):
    return np.cos(x + 1)


x = np.linspace(1, 10, 100)
f_0 = f(x)
plt.plot(x, np.sin(x), 'r^', label='$\sin(x)$')
plt.plot(x, f_0, 'b--', label='$\cos(x+1)$')
plt.xlabel('$x$')
plt.ylabel('y')
plt.title('Sin and Cos')
plt.legend()
plt.tight_layout()
plt.savefig('plot.png')
plt.show()
